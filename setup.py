#!/usr/bin/env python3
# import
from setuptools import setup, find_packages
# define setup parameters
# for version norm, see : https://www.python.org/dev/peps/pep-0440/#post-releases
setup(
    name="PythonCommonTools",
    version="0.0.2",
    description="common tools for Python",
    packages=find_packages(),
    install_requires=["psutil"],
    classifiers=[
        'Programming Language :: Python :: 3',
    ],
)
