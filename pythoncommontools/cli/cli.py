# coding=utf-8
# import
from gzip import decompress, compress
from json import loads
from pythoncommontools.file.file import writeToFile as writeToFileDefault, readFromFile as readFromFileDefault
# display extra help
def displayExtraHelp(extraHelpFile):
    extraHelp=readFromFileDefault(extraHelpFile,'r')
    print(extraHelp)
# handle response
def executeCommandCheckResponse(command,URL,data=None):
    response = command(URL, data=data)
    assert response.status_code==200, str(response.status_code) + ' ' + response.reason + " : " + response.text
    return response
# decode response
def decodeReponse(response):
    decodedResponse = loads(decompress(response.content).decode())
    return decodedResponse
# write to file
def writeToFile(filePath,compressedContent):
    decompressedContent = decompress(compressedContent)
    writeToFileDefault(filePath,decompressedContent)
# read from file
def readFromFile(filePath):
    decompressedContent = readFromFileDefault(filePath)
    compressedContent = compress(decompressedContent)
    return compressedContent
# arguments exception
argumentsException = "ERROR : command does not match"
def raiseArgumentsException():
    raise Exception(argumentsException)
# main
def main(argv, parser, dispatchRequest, extraHelpFile = None):
    # INFO : remove first argument, it is the script name
    args = list(argv)
    del args [0]
    if extraHelpFile and (len(args)==1 or args[1].upper() in ("-H","--HELP",)):
        displayExtraHelp(extraHelpFile)
    arguments = parser.parse_args(args)
    response = dispatchRequest(arguments)
    return response
pass