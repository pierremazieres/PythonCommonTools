# coding=utf-8
# import
from unittest import TestCase as ParentTestCase
# test utilities
class TestCase(ParentTestCase):
    def assertListAlmostEqual(self,expectedList,actualList,label='',acceptanceScale=1e-15):
        self.assertEqual(len(expectedList), len(actualList), "ERROR : length "+label+" does not match")
        for index, expectedElement in enumerate(expectedList):
            actualElement = actualList[index]
            deviation = abs(expectedElement-actualElement)
            self.assertAlmostEqual(expectedElement, actualElement, msg="ERROR : index#"+str(index)+' '+label+" does not match : delta="+str(deviation),delta=acceptanceScale)
            pass
        pass
    pass
pass