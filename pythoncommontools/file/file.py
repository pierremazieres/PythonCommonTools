# coding=utf-8
# import
from tempfile import NamedTemporaryFile
# file utils
def writeTemporaryFile(data,temporaryFile=None):
    # close (& delete) temporary files, if you want to reuse it
    if temporaryFile and not temporaryFile.closed:
        temporaryFile.close()
    # open & write temporary files
    temporaryFile = NamedTemporaryFile()
    temporaryFile.write(data)
    temporaryFile.seek(0)
    return temporaryFile
# write to file
def writeToFile(filePath,content,mode="wb"):
    file = open(filePath, mode)
    file.write(content)
    file.close()
# read from file
def readFromFile(filePath,mode="rb"):
    file = open(filePath, mode)
    content=file.read()
    file.close()
    return content
pass
