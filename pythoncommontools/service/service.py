# coding=utf-8
# import
from enum import Enum, unique
from flask import Flask, Response, jsonify
from flask_restful import Api
from gzip import decompress, compress
from json import loads, dumps
from logging import StreamHandler, Formatter, getLogger
from pythoncommontools.logger.logger import CommonLogMarkup
from pythoncommontools.service.httpSymbol import HttpSymbol
# contants
@unique
class Action(Enum):
    GET = "get"
    POST = "post"
    PUT = "put"
    DELETE = "delete"
    PATCH = "patch"
@unique
class CommonLogFormatElement(Enum):
    METHOD = "method"
    RESOURCE = "resource"
# initialize service
''' TODO :
 - add login/password (in order to access only user resources)
 - when an insert/update method fail, send request content to a dedicated document DB (couchbase ?)
 - on endpoint get page : dynamic display all ressources & relatif path
 - on each ressources get page : display JSON description'''
application = Flask(__name__)
API = Api(application)
# set log
formatter = Formatter("[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s")
handler = StreamHandler()
handler.setFormatter(formatter)
application.logger.addHandler(handler)
log = getLogger("werkzeug")
log.addHandler(handler)
# utilities
# INFO : log data can be > 100Mb so we do not log them
def inputLogFormator(method,resource,parameters):
    dictionnary = {
        CommonLogFormatElement.METHOD.value : method.value,
        CommonLogFormatElement.RESOURCE.value : resource,
        "parameters" : parameters,
    }
    message = CommonLogMarkup.INPUT.value + " : " + dumps(dictionnary)
    return message
def outputLogFormator(method,resource):
    dictionnary = {
        CommonLogFormatElement.METHOD.value : method.value,
        CommonLogFormatElement.RESOURCE.value : resource,
    }
    message = CommonLogMarkup.OUTPUT.value + " : " + str(dictionnary)
    return message
def errorLogFormator(method,resource,error):
    dictionnary = {
        CommonLogFormatElement.METHOD.value : method.value,
        CommonLogFormatElement.RESOURCE.value : resource,
        "error": error,
    }
    return str(dictionnary)
def responseError(errorCode,exception):
    responseError = jsonify(exception.args)
    responseError.status_code = errorCode
    return responseError
def loadRequest(request):
    parsedData = loads(decompress(request.data).decode())
    return parsedData
def dumpResponse(json):
    response = Response(compress(dumps(json).encode()))
    return response
# construct URL
def constructUrl(endpoint,resourcePath,specificResourceMembers=None,parameters=None):
    url = endpoint+HttpSymbol.PATH_SEPARATOR.value+resourcePath
    if specificResourceMembers:
        for key, value in specificResourceMembers.items():
            url = url.replace(key, str(value))
    if parameters:
        firstParameter = True
        for key, value in parameters.items():
            if firstParameter:
                url += HttpSymbol.PARAMETER_STARTER.value
                firstParameter = False
            else :
                url += HttpSymbol.PARAMETER_SEPARATOR.value
            url += key + HttpSymbol.PARAMETER_EQUALITY.value + str(value)
    return url
pass
