# coding=utf-8
# import
from enum import Enum, unique
# http symbols
@unique
class HttpSymbol(Enum):
    PATH_SEPARATOR = '/'
    PARAMETER_STARTER = '?'
    PARAMETER_SEPARATOR = '&'
    PARAMETER_EQUALITY = '='
    ARRAY_SEPARATOR = ','
    LITERAL_SEPARATOR = '"'
    PORT_SEPARATOR = ':'
    CLEAR_PROTOCOL = "http://"
    pass
pass
# create endpoint URL
def getServerUrl(host,port):
    return HttpSymbol.CLEAR_PROTOCOL.value+host+HttpSymbol.PORT_SEPARATOR.value+str(port)+HttpSymbol.PATH_SEPARATOR.value
# WARNING : we assume no double-quotes are already in URL
def enquoteValue(value,quoteChar=HttpSymbol.LITERAL_SEPARATOR.value):
    enquotedValue=str(value)
    if not enquotedValue.startswith(quoteChar):
        enquotedValue = HttpSymbol.LITERAL_SEPARATOR.value + enquotedValue
    if not enquotedValue.endswith(quoteChar):
        enquotedValue = enquotedValue + HttpSymbol.LITERAL_SEPARATOR.value
    return enquotedValue
def dequoteString(quotedString,quoteChar=HttpSymbol.LITERAL_SEPARATOR.value):
    dequotedValue=str(quotedString).replace(quoteChar,'')
    return dequotedValue
def arrayToString(array):
    string = HttpSymbol.ARRAY_SEPARATOR.value.join([str(_) for _ in array])
    return string
def stringToArray(string,convertionFunction=None):
    array = string.split(HttpSymbol.ARRAY_SEPARATOR.value)
    if convertionFunction:
        array = [convertionFunction(_) for _ in array]
    return array
def arrayToQuotedString(array,quoteChar=HttpSymbol.LITERAL_SEPARATOR.value):
    quotedString = arrayToString(array)
    quotedString = enquoteValue(quotedString,quoteChar)
    return quotedString
def quotedStringToArray(quotedString,quoteChar=HttpSymbol.LITERAL_SEPARATOR.value,convertionFunction=None):
    array = dequoteString(quotedString,quoteChar)
    array = stringToArray(array,convertionFunction)
    return array
pass