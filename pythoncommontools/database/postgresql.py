# coding=utf-8
# import
from pythoncommontools.database.database import randomDbObjectName
from abc import ABC
# read and process row table one-by-one
class PostgreSqlUnitaryReadProcessRowTable(ABC):
    # methods
    def getNextRow(self):
        self.clientCursor.execute("FETCH 1 FROM " + self.serverCursor)
        nextRow = self.clientCursor.fetchone()
        return nextRow
    def getFisrstRow(self,statement,parameters):
        self.clientCursor.execute(statement,parameters)
        firstRow = self.getNextRow()
        return firstRow
    def closeServerCursor(self):
        self.clientCursor.execute("CLOSE " + self.serverCursor)
    # constructors
    def __init__(self,cursorConnection):
        self.serverCursor = randomDbObjectName()
        self.cursorConnection = cursorConnection
        self.clientCursor = self.cursorConnection.cursor()
    def cleanup(self):
        self.clientCursor.close()
        self.cursorConnection.close()
    pass
pass
