# coding=utf-8
# import
from uuid import uuid4
# random plan name
# INFO : to avoid plan collision, every connection have its own one
def randomDbObjectName():
    return "rDBn"+str(uuid4()).replace('-','')
pass
