# coding=utf-8
# import
# services test utilities
def checkRestClientDefaultError(testInstance, main,args, parser, dispatchRequest):
    raisedError = True
    try:
        main(args, parser, dispatchRequest)
        raisedError = False
    except Exception as error:
        testInstance.assertIn("INTERNAL SERVER ERROR", str(error).upper(), "ERROR : Error message not expected")
    finally:
        testInstance.assertTrue(raisedError, "ERROR : Error not raised")
    pass
pass
