# coding=utf-8
# import
def checkPostgreSqlDatabaseError(testInstance,function,parameters):
    try:
        function(*parameters)
        raise Exception()
    except Exception as exception:
        testInstance.assertIsNotNone(exception.pgerror, "ERROR : Exception not raised")
    pass
