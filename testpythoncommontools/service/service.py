# coding=utf-8
# import
from json import loads
# services test utilities
def checkRestServerNotDefaultError(testInstance, function ,resource, data=None):
    response = function(resource, data=data)
    testInstance.assertEqual(response.status_code, 500, "ERROR : Status code not expected")
    error = loads(response.data.decode())
    testInstance.assertNotIn("INTERNAL SERVER ERROR", error[0].upper(), "ERROR : Error message not expected")
    pass
pass
